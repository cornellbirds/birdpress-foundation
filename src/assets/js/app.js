jQuery(document).foundation();

//this following event watcher is triggered for redrawing the carousels embedded in Foundation Reveal Modals
jQuery('.reveal').on('open.zf.reveal', function() {
	jQuery('.slick-1').slick("setPosition",0);
});
