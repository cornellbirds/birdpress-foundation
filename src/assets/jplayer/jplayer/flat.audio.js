jQuery(document).ready(function ($) {
    var count = 1;
    jQuery.each(jQuery(".jp-jplayer, player-audio"), function (i, item) {
        jQuery(item).attr('id', jQuery(item).attr('id') + "_" + count);
        count = count + 1;
    });
    var count = 1;
    jQuery.each(jQuery(".jp-flat-audio"), function (i, item) {
        jQuery(item).attr('id', jQuery(item).attr('id') + "_" + count);
        count = count + 1;
    });
    count = count - 1;
    while (count > 0) {
        $("#jquery_jplayer_audio_" + count).jPlayer({
		    ready: function () {
			    $(this).jPlayer("setMedia", {
				    mp3: jQuery(this).attr('name'),
			    });
		    },
		    play: function() { // To avoid multiple jPlayers playing together.
			    $(this).jPlayer("pauseOthers");
		    },
		    timeFormat: {
                padMin: false
            },
            swfPath: "../js",
            supplied: "mp3",
            cssSelectorAncestor: "#jp_container_audio_" + count,
            useStateClassSkin: true,
            autoBlur: false,
            smoothPlayBar: true,
            remainingDuration: true,
            keyEnabled: true,
            keyBindings: {
                // Disable some of the default key controls
                loop: null,
                muted: null,
                volumeUp: null,
                volumeDown: null
            },
            wmode: "window"
	    });
        count = count - 1;
    }

});
